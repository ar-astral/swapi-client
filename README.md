Descripción,

La aplicación a sido desarrollada entorno al marco de ejecución Symfony 5.1.
Utilizando Doctrine ORM para la administración de la basa de datos,
Twig para el renderizado html,
EasyAdmin para las pantallas de administración de contenido,
ApiPlatform para el desarrollo de la APi REST.

La distribución de producción ha sido empaquetada como un contenedor docker,
el cual provee un entorno de ejecución para Php y el servidor integrado Nginx.

A modo de simplificación la base de datos utilizada es el motor Sqlite, opcionalmente
podría utilizarse MySql, o SqlServer con pequeña modificación.

Instalación:

* Requisitos Previos:
	- Docker
	- Docker Compose

* Opcional:
	- Php 7.4
	- Composer
	- Symfony Cli

Descomprimir la carpeta con el contenido,
Desde un terminal acceder a la carpeta raiz y luego ejecutar el siguiente comando:

<carpeta_raiz>\docker: docker-compose -up

Esperar a que sean ejecutados la carga de los contenedores, luego de iniciado desde un navegador
acceder a http:\\localhost

Opcionalmente puede utilizarse el cliente symfony para la ejecución de prueba.
Para ello, previamente instalados Php, Composer y Symfony Cli, acceder a la carpeta raiz y
luego ejecutar el siguiente comando:

<carpeta_raiz>\src: symfony composer install
<carpeta_raiz>\src: symfony server:start

Luego desde un navegador acceder a la siguiente dirección (puerto por defecto 8000):
http:\\localhost:8000
