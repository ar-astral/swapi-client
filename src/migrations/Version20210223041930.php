<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210223041930 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE starship (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, model VARCHAR(255) NOT NULL, manufacturer VARCHAR(255) NOT NULL, cost_in_credits VARCHAR(255) NOT NULL, max_atmosphering_speed VARCHAR(255) NOT NULL, length VARCHAR(255) NOT NULL, crew VARCHAR(255) NOT NULL, passengers VARCHAR(255) NOT NULL, cargo_capacity VARCHAR(255) NOT NULL, consumables VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, pilots CLOB NOT NULL --(DC2Type:array)
        , films CLOB NOT NULL --(DC2Type:array)
        , created DATETIME NOT NULL, edited DATETIME NOT NULL, count INTEGER NOT NULL, hyperdrive_rating VARCHAR(255) NOT NULL, mglt VARCHAR(255) NOT NULL, starship_class VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE vehicle (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, model VARCHAR(255) NOT NULL, manufacturer VARCHAR(255) NOT NULL, cost_in_credits VARCHAR(255) NOT NULL, max_atmosphering_speed VARCHAR(255) NOT NULL, length VARCHAR(255) NOT NULL, crew VARCHAR(255) NOT NULL, passengers VARCHAR(255) NOT NULL, cargo_capacity VARCHAR(255) NOT NULL, consumables VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, pilots CLOB NOT NULL --(DC2Type:array)
        , films CLOB NOT NULL --(DC2Type:array)
        , created DATETIME NOT NULL, edited DATETIME NOT NULL, count INTEGER NOT NULL, vehicle_class VARCHAR(255) NOT NULL)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE starship');
        $this->addSql('DROP TABLE vehicle');
    }
}
