<?php
namespace App\Controller\Admin;

use App\Entity\Starship;

class StarshipCrudController extends VesselCrudController
{
    public static function getEntityFqcn(): string
    {
        return Starship::class;
    }

}
