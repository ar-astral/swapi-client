<?php
namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Security\Permission;
use EasyCorp\Bundle\EasyAdminBundle\Exception\ForbiddenActionException;
use EasyCorp\Bundle\EasyAdminBundle\Exception\InsufficientEntityPermissionException;
use EasyCorp\Bundle\EasyAdminBundle\Factory\EntityFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use App\Dto\Cantidad;

use App\Form\ModifyStockType;
use App\Form\ConfirmType;

use App\Service\Inventory;


abstract class VesselCrudController extends AbstractCrudController
{
    /**
     * Administrador de Inventario.
     *
     * @var Inventory
     */
    private $inventory;

    public function __construct(Inventory $inventory)
    {
        $this->inventory = $inventory;
    }
    public function configureFields(string $pageName): iterable
    {
        if (Crud::PAGE_INDEX === $pageName) 
            return [
                'id',
                'name',
                'model',
                'manufacturer',
                'cost_in_credits',
                'count',
            ];
        return parent::configureFields($pageName);
    }

    public function configureActions(Actions $actions): Actions
    {
        $modifyStockAction = Action::new('modifyStock', '+ / -');
        $modifyStockAction->linkToCrudAction('modifyStock');
        $modifyStockButton = Action::new('modifyStock', 'Modificar Stock');
        $modifyStockButton->linkToCrudAction('modifyStock');
        $modifyStockButton->addCssClass('btn btn-secondary');
        $truncateAction = Action::new('truncate', 'Truncar Tabla');
        $truncateAction->linkToCrudAction('truncate');
        $truncateAction->createAsGlobalAction();
        $truncateAction->addCssClass('btn btn-danger');

        return $actions
            ->add(Crud::PAGE_INDEX, $modifyStockAction)
            ->add(Crud::PAGE_INDEX, $truncateAction)
            ->add(Crud::PAGE_EDIT, $modifyStockButton);
    }

    public function modifyStock(AdminContext $context)
    {

        $event = new BeforeCrudActionEvent($context);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        if (!$this->isGranted(Permission::EA_EXECUTE_ACTION)) {
            throw new ForbiddenActionException($context);
        }

        if (!$context->getEntity()->isAccessible()) {
            throw new InsufficientEntityPermissionException($context);
        }

        $actions = Actions::new();
        $actions->add(Crud::PAGE_DETAIL, Action::EDIT);
        $actions->add(Crud::PAGE_DETAIL, Action::INDEX);
        $context->getCrud()->setPageName(Crud::PAGE_DETAIL);
        $this->get(EntityFactory::class)->processActions($context->getEntity(), $actions->getAsDto(Crud::PAGE_DETAIL));

        $vessel = $context->getEntity()->getInstance();
        $cantidad = new Cantidad();
        $form = $this->createForm(ModifyStockType::class, $cantidad);
        $form->handleRequest($context->getRequest());
        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $cantidad = $form->getData();
            try {
                /// https://github.com/symfony/symfony-docs/issues/11777
                /// https://symfony.com/doc/current/form/multiple_buttons.html
                if($form->get('incrementar')->isClicked())
                {
                    $event = new BeforeEntityUpdatedEvent($vessel);
                    $this->get('event_dispatcher')->dispatch($event);
                    $vessel = $event->getEntityInstance();

                    $this->inventory->incrementarStock($vessel, $cantidad->getCantidad());
                } else if($form->get('disminuir')->isClicked())
                {
                    $event = new BeforeEntityUpdatedEvent($vessel);
                    $this->get('event_dispatcher')->dispatch($event);
                    $vessel = $event->getEntityInstance();
                    $this->inventory->disminuirStock($vessel, $cantidad->getCantidad());
                }
                
            } catch (\Exception $ex)
            {
                $error = $ex;
            }
        }
        
        // add your logic here...
        $responseParameters = $this->configureResponseParameters(KeyValueStore::new([
            'pageName' => 'modifyStock',
            'form' => $form,
            'entity' => $context->getEntity(),
        ]));

        $event = new AfterCrudActionEvent($context, $responseParameters);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        return $this->render(
            'admin/vessel/vessel_modify_stock.html.twig', 
            [
                'vessel' => $vessel,
                'entity' => $context->getEntity(),
                'form' => $form->createView(),
                'error' => $error,
            ]);
    }

    public function truncate(AdminContext $context)
    {

        $event = new BeforeCrudActionEvent($context);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        if (!$this->isGranted(Permission::EA_EXECUTE_ACTION)) {
            throw new ForbiddenActionException($context);
        }

        if (!$context->getEntity()->isAccessible()) {
            throw new InsufficientEntityPermissionException($context);
        }

        $form = $this->createForm(ConfirmType::class);
        $form->handleRequest($context->getRequest());
        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            /// https://github.com/symfony/symfony-docs/issues/11777
            /// https://symfony.com/doc/current/form/multiple_buttons.html
            if($form->get('yes')->isClicked())
            {
                $this->inventory->eliminarDatos($this->getEntityFqcn());
                    /// https://stackoverflow.com/questions/63312446/how-to-add-a-custom-action-in-easyadmin-3
                $this->addFlash('success', 'La tabla ha sido truncada.');
            }
            return $this->redirect($this->get(AdminUrlGenerator::class)->setAction(Action::INDEX)->generateUrl());
        }
        
        // add your logic here...
        $responseParameters = $this->configureResponseParameters(KeyValueStore::new([
            'pageName' => 'truncate',
            'form' => $form,
            'class' => $this->getEntityFqcn()
        ]));

        $event = new AfterCrudActionEvent($context, $responseParameters);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        return $this->render(
            'admin/vessel/truncate.html.twig', 
            [
                'class' => $this->getEntityFqcn(),
                'form' => $form->createView()
            ]);
    }
    
}
