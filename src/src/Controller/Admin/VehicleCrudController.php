<?php
namespace App\Controller\Admin;

use App\Entity\Vehicle;

class VehicleCrudController extends VesselCrudController
{
    public static function getEntityFqcn(): string
    {
        return Vehicle::class;
    }

}
