<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Starship;
use App\Entity\Vehicle;

use App\Service\Inventory;

use App\Model\ImportarDatosQuery;
use App\Form\ImportarDatosQueryType;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    /**
     * @Route("/admin/config", name="admin_config")
     */
    public function config(Inventory $inventory, Request $request): Response
    {

        $importarDatosQuery = new ImportarDatosQuery();

        $form = $this->createForm(ImportarDatosQueryType::class, $importarDatosQuery);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $importarDatosQuery = $form->getData();
            $inventory->importarDatos($importarDatosQuery);
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            // $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->persist($task);
            // $entityManager->flush();

            return $this->render('admin/config_datos_importados.html.twig', ['form' => $form->createView()]);
        }

        //dump($inventory->fetchResource(Starship::class));
        // you can also render some template to display a proper Dashboard
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        return $this->render('admin/config.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('SWAPI');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Naves Espaciales', 'fas fa-rocket', Starship::class);
        yield MenuItem::linkToCrud('Vehículos', 'fas fa-space-shuttle', Vehicle::class);
        yield MenuItem::linktoRoute('Configurar', 'fa fa-cubes', 'admin_config');
        yield MenuItem::section();
        yield MenuItem::linkToUrl('Api', 'fa fa-book', '/api');
        yield MenuItem::linkToUrl('Visitar SWAPI Website', 'fab fa-google', 'https://swapi.dev/documentation');
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
