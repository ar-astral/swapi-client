<?php
// src/DataTransformer/BookInputDataTransformer.php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use App\Dto\Incrementar;
use App\Entity\Vessel;

use App\Service\Inventory;
final class IncrementarDataTransformer implements DataTransformerInterface
{
    /**
     * Administrador de Inventario.
     *
     * @var Inventory
     */
    private $inventory;
    public function __construct(Inventory $inventory)
    {
        $this->inventory = $inventory;
    }
    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        $vessel = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];
        $this->inventory->incrementarStock($vessel, $data->getCantidad());
        return $vessel;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        
        if($data instanceof Vessel) {
          return false;
        }
        return  is_subclass_of($to, Vessel::class) && Incrementar::class === ($context['input']['class'] ?? null);
    }
}