<?php
namespace App\Model;

use App\Entity\Starship;

class StarshipResponse extends RemoteResponse
{

    /**
     * @var Starship[] results
     */
    public $results;
    
}