<?php
namespace App\Model;

use App\Entity\Vehicle;

class VehicleResponse extends RemoteResponse
{
    /**
     * @var Vehicle[] results
     */
    public $results;
    
}