<?php
namespace App\Model;

/**
 * Representa la respuesta remota del servidor swapi a raiz
 * de la petición de un recurso.
 */
abstract class RemoteResponse
{
    /**
     * @var int count
     */
    public $count;

    /**
     * @var string|null next
     */
    public $next;

    /**
     * @var string|null previous
     */
    public $previous;
}