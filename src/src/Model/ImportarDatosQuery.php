<?php
namespace App\Model;

class ImportarDatosQuery
{
    private $vehicles;
    private $starships;

    public function getVehicles(): ?bool
    {
        return $this->vehicles;
    }
    
    public function setVehicles(bool $vehicles): self
    {
        $this->vehicles = $vehicles;

        return $this;
    }

    public function getStarships(): ?bool
    {
        return $this->starships;
    }

    public function setStarships(bool $starships): self
    {
        $this->starships = $starships;

        return $this;
    }

}