<?php
namespace App\Dto;
use Symfony\Component\Validator\Constraints as Assert;
class Cantidad {
    /**
     * Almacena la cantidad a modificar en el stock.
     * @Assert\PositiveOrZero
     *
     * @var int Cantidad a modificar en el stock.
     */
    private $cantidad;
    
    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;
        return $this;
    }
}