<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\VehicleRepository;
use App\Dto\Incrementar;
use App\Dto\Disminuir;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *  itemOperations=
 *  {
 *      "get",
 *      "put",
 *      "patch",
 *      "delete",
 *      "incrementar" = {
 *          "messenger" = "input",
 *          "input" = Incrementar::class, 
 *          "method" = "PATCH", 
 *          "path" = "/vehicles/{id}/incrementar"
 *     },
 *     "disminuir" = {
 *          "messenger" = "input", 
 *          "input" = Disminuir::class, 
 *          "method" = "PATCH", 
 *          "path" = "/vehicles/{id}/disminuir"
 *     }
 *  }
 * )
 * @ORM\Entity(repositoryClass=VehicleRepository::class)
 */
class Vehicle extends Vessel
{
    /**
     * The class of this vehicle, such as Wheeled.
     * @ORM\Column(type="string", length=255)
     */
    private $vehicle_class;

    public function getVehicleClass(): ?string
    {
        return $this->vehicle_class;
    }

    public function setVehicleClass(string $vehicle_class): self
    {
        $this->vehicle_class = $vehicle_class;

        return $this;
    }
}
