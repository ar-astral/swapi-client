<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StarshipRepository;
use App\Dto\Incrementar;
use App\Dto\Disminuir;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;


/**
 * @ApiResource(
 * itemOperations=
 *  {
 *      "get",
 *      "put",
 *      "patch",
 *      "delete",
 *      "incrementar" = {
 *          "messenger" = "input",
 *          "input" = Incrementar::class, 
 *          "method" = "PATCH", 
 *          "path" = "/starships/{id}/incrementar"
 *     },
 *     "disminuir" = {
 *          "messenger" = "input", 
 *          "input" = Disminuir::class, 
 *          "method" = "PATCH", 
 *          "path" = "/starships/{id}/disminuir"
 *     }
 *  }
 * )
 * @ORM\Entity(repositoryClass=StarshipRepository::class)
 */
class Starship extends Vessel
{
    /**
     * The class of this starships hyperdrive.
     * @ORM\Column(type="string", length=255)
     */
    private $hyperdrive_rating;

    /**
     * The Maximum number of Megalights this starship can travel in a standard hour. A Megalight is a standard unit of distance and has never been defined before within the Star Wars universe. This figure is only really useful for measuring the difference in speed of starships. We can assume it is similar to AU, the distance between our Sun (Sol) and Earth.
     * @ORM\Column(type="string", length=255)
     * @SerializedName("MGLT")
     */
    private $MGLT;

    /**
     * The class of this starship, such as Starfighter or Deep Space Mobile Battlestation.
     * @ORM\Column(type="string", length=255)
     */
    private $starship_class;

    public function getHyperdriveRating(): ?string
    {
        return $this->hyperdrive_rating;
    }

    public function setHyperdriveRating(string $hyperdrive_rating): self
    {
        $this->hyperdrive_rating = $hyperdrive_rating;

        return $this;
    }

    public function getMGLT(): ?string
    {
        return $this->MGLT;
    }

    public function setMGLT(string $MGLT): self
    {
        $this->MGLT = $MGLT;

        return $this;
    }

    public function getStarshipClass(): ?string
    {
        return $this->starship_class;
    }

    public function setStarshipClass(string $starship_class): self
    {
        $this->starship_class = $starship_class;

        return $this;
    }
}
