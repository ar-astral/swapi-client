<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Clase Base de Vehículos y Naves Espaciales.
 * @ORM\MappedSuperclass
 */
abstract class Vessel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * The name of this vehicle. The common name, such as Sand Crawler.
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * The model or official name of this vehicle. Such as All Terrain Attack Transport.
     * @ORM\Column(type="string", length=255)
     */
    private $model;

    /**
     * The manufacturer of this vehicle. Comma seperated if more than one.
     * @ORM\Column(type="string", length=255)
     */
    private $manufacturer;

    /**
     * The cost of this vehicle new, in galactic credits."
     * @ORM\Column(type="string", length=255)
     */
    private $cost_in_credits;

    /**
     * The maximum speed of this starship in atmosphere. n/a if this starship is incapable of atmosphering flight.
     * @ORM\Column(type="string", length=255)
     */
    private $max_atmosphering_speed;

    /**
     * The length of this vehicle in meters.
     * @ORM\Column(type="string", length=255)
     */
    private $length;

    /**
     * The number of personnel needed to run or pilot this vehicle.
     * @ORM\Column(type="string", length=255)
     */
    private $crew;

    /**
     * The number of non-essential people this vehicle can transport.
     * @ORM\Column(type="string", length=255)
     */
    private $passengers;

    /**
     * The maximum number of kilograms that this vehicle can transport.
     * @ORM\Column(type="string", length=255)
     */
    private $cargo_capacity;

    /**
     * The maximum length of time that this vehicle can provide consumables for its entire crew without having to resupply.
     * @ORM\Column(type="string", length=255)
     */
    private $consumables;

    /**
     * The hypermedia URL of this resource.
     * @ORM\Column(type="string", length=255)
     * @Assert\Url
     */
    private $url;

    /**
     * An array of People URL Resources that this vehicle has been piloted by.
     * @ORM\Column(type="array")
     */
    private $pilots = [];

    /**
     * An array of Film URL Resources that this vehicle has appeared in.
     * @ORM\Column(type="array")
     */
    private $films = [];

    /**
     * The ISO 8601 date format of the time that this resource was created.
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * the ISO 8601 date format of the time that this resource was edited."
     * @ORM\Column(type="datetime")
     */
    private $edited;

    /**
     * Units in stock.
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero
     */
    private $count;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->edited = new \DateTime();
        $this->count = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getManufacturer(): ?string
    {
        return $this->manufacturer;
    }

    public function setManufacturer(string $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getCostInCredits(): ?string
    {
        return $this->cost_in_credits;
    }

    public function setCostInCredits(string $cost_in_credits): self
    {
        $this->cost_in_credits = $cost_in_credits;

        return $this;
    }

    public function getMaxAtmospheringSpeed(): ?string
    {
        return $this->max_atmosphering_speed;
    }

    public function setMaxAtmospheringSpeed(string $max_atmosphering_speed): self
    {
        $this->max_atmosphering_speed = $max_atmosphering_speed;

        return $this;
    }

    public function getLength(): ?string
    {
        return $this->length;
    }

    public function setLength(string $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getCrew(): ?string
    {
        return $this->crew;
    }

    public function setCrew(string $crew): self
    {
        $this->crew = $crew;

        return $this;
    }

    public function getPassengers(): ?string
    {
        return $this->passengers;
    }

    public function setPassengers(string $passengers): self
    {
        $this->passengers = $passengers;

        return $this;
    }

    public function getCargoCapacity(): ?string
    {
        return $this->cargo_capacity;
    }

    public function setCargoCapacity(string $cargo_capacity): self
    {
        $this->cargo_capacity = $cargo_capacity;

        return $this;
    }

    public function getConsumables(): ?string
    {
        return $this->consumables;
    }

    public function setConsumables(string $consumables): self
    {
        $this->consumables = $consumables;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPilots(): ?array
    {
        return $this->pilots;
    }

    public function setPilots(array $pilots): self
    {
        $this->pilots = $pilots;

        return $this;
    }

    public function getFilms(): ?array
    {
        return $this->films;
    }

    public function setFilms(array $films): self
    {
        $this->films = $films;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getEdited(): ?\DateTimeInterface
    {
        return $this->edited;
    }

    public function setEdited(\DateTimeInterface $edited): self
    {
        $this->edited = $edited;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }
}
