<?php
namespace App\Service;

/**
 * Excepción originada cuando una clase no es soportada por una operación.
 */
class ClaseInvalidaEx extends AppException
{

    /**
     * Clase que ha originado la excepción.
     *
     * @var string
     */
    private $unsupportedClass;

    /**
     * Clase requerida.
     *
     * @var string
     */
    private $requiredClass;

    public function __construct(string $unsupportedClass, string $requiredClass){
        parent::__construct('La clase '. $unsupportedClass . ' no es soportada. La clase dada requiere heredar de ' . $requiredClass);
        $this->unsupportedClass = $unsupportedClass;
        $this->requiredClass = $requiredClass;
    } 

    /**
     * Obtiene el nombre de la clase no soportada que ha originado la excepción.
     *
     * @return integer el nombre de la clase no soportada que ha originado la excepción.
     */
    public function getUnsupportedClass(): string
    {
        return $this->unsupportedClass;
    }

    /**
     * Obtiene el nombre de la clase requerida.
     *
     * @return integer el nombre de la clase requerida.
     */
    public function getRequiredClass(): string
    {
        return $this->requiredClass;
    }

    /**
     * Comprueba si la clase dada hereda de la clase requerida.
     *
     * @param string $class clase a comprobar.
     * @param string $requiredClass clase requerida.
     * @return string el nombre de la clase a comprobar si hereda de la clase requerida.
     * 
     * @throws ClaseInvalidaEx si la clase dada no hereda de la clase requerida.
     */
    public static function check(string $class, string $requiredClass): string
    {
        if (!is_subclass_of($class, $requiredClass))
            throw new ClaseInvalidaEx($class, $requiredClass);
        return $class;
    }

}