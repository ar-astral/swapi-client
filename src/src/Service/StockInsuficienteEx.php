<?php
namespace App\Service;

use App\Entity\Vessel;

/**
 * Excepción que ocurre cuando el stock de Naves Espaciales o Vehículos es insuficiente.
 */
class StockInsuficienteEx extends AppException
{

    /**
     * Nave Espacial o Vehículo del cual se ha intenado disminuir el stock.
     * @var Vessel
     */
    private $vessel;
    /**
     * Valor que ha originado la excepción.
     *
     * @var integer 
     */
    private $cantidad;
    public function __construct(Vessel $vessel, int $cantidad){
        parent::__construct('Stock insuficiente. Stock disponible: ' . $vessel->getCount() . ', Valor a disminuir dado: ' . $cantidad);
        $this->cantidad = $cantidad;
    } 

    /**
     * Devuelve la Nave Espacial o Vehículo sobre la cual se ha intentado disminuir el stock.
     *
     * @return Vessel
     */
    public function getVessel(): Vessel
    {
        return $this->vessel;
    }

    /**
     * Obtiene la cantidad que ha originado la excepción.
     *
     * @return integer el valor que ha originado la excepción.
     */
    public function getCantidad(): int
    {
        return $this->cantidad;
    }

    /**
     * Verifica que el stock de unidades sea suficiente para disminuir la cantidad dada.
     * @param Vessel Nave Espacial o Vehículo del cual se ha intenado disminuir el stock.
     * @param integer $cantidad valor a verificar.
     * @return integer la cantidad ingresada luego de verificar si es válida.
     * 
     * @throws CantidadInvalidaEx si el valor ingresado es un número negativo.
     * @throws StockInsuficienteEx si el stock de unidades es insuficiente para disminuir la cantidad dada.
     * 
     */
    public static function check(Vessel $vessel, int $cantidad): int
    {
        $cantidadDisminuida = $vessel->getCount() - CantidadInvalidaEx::check($cantidad);
        if ($cantidadDisminuida < 0)
            throw new StockInsuficienteEx($vessel, $cantidad);
        return $cantidad;
    }
}