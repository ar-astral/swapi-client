<?php
namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;


use App\Entity\Starship;
use App\Entity\Vehicle;

use App\Model\StarshipResponse;
use App\Model\VehicleResponse;

use App\Model\ImportarDatosQuery;
use App\Entity\Vessel;
/**
 * Administra el inventario de naves espaciales y vehículos.
 */
class Inventory 
{
    /**
     * Ruta remota del recurso Starship.
     */
    public const STARSHIPS = 'starships';

    /**
     * Ruta remota del recurso Vehicle.
     */
    public const VEHICLES = 'vehicles';

    /**
     * Mapa de recursos y rutas.
     */
    public const RESOURCE = [
        Starship::class => self::STARSHIPS,
        Vehicle::class => self::VEHICLES
    ];

    /**
     * Mapa de recursos y respuestas.
     */
    public const RESOURCE_RESPONSE = [
        Starship::class => StarshipResponse::class,
        Vehicle::class => VehicleResponse::class
    ];

    protected $container;
    protected $denormalizer;
    protected $serializer;
    protected $entityManager;
    protected $swapiUrl;

    public function __construct(ContainerInterface $container, DenormalizerInterface $denormalizer, SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {
        $this->container = $container;
        $this->denormalizer = $denormalizer;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->swapiUrl = $container->getParameter('app.inventory.swapi_url');
    }

    /**
     * Incrementa el stock de unidades.
     *
     * @param Vessel $vessel Vehículo o Nave Espacial al cual se incrementarán las unidades.
     * @param integer $cantidad Unidades a incrementar.
     * @return Vessel la instancia del Vehículo o Nave Espacial dado.
     * 
     * @throws CantidadInvalidaEx si el valor ingresado es un número negativo.
     */
    public function incrementarStock(Vessel $vessel, int $cantidad): Vessel
    {
        CantidadInvalidaEx::check($cantidad);
        $vessel->setCount($vessel->getCount() + $cantidad);
        $this->entityManager->persist($vessel);
        $this->entityManager->flush();
        return $vessel;
    }

    /**
     * Disminuye el stock de unidades.
     *
     * @param Vessel $vessel Vehículo o Nave Espacial del cual se disminuiran las unidades.
     * @param integer $cantidad unidades a disminuir.
     * @return Vessel la instancia del Vehículo o Nave Espacial dado.
     * 
     * @throws CantidadInvalidaEx si el valor ingresado es un número negativo.
     * @throws StockInsuficienteEx si el stock de unidades es insuficiente para disminuir la cantidad dada.
     */
    public function disminuirStock(Vessel $vessel, int $cantidad): Vessel
    {
        StockInsuficienteEx::check($vessel, $cantidad);
        $vessel->setCount($vessel->getCount() - $cantidad);
        $this->entityManager->persist($vessel);
        $this->entityManager->flush();
        return $vessel;
    }

    /**
     * Eliminar los datos de la tablas correspondientes a Vehículos o Naves Espaciales.
     * La clase dada requiere heredar de la clase base Vessel.
     *
     * @param String $vesselClass
     * @return void
     * 
     * @throws ClaseInvalidaEx si la clase dada no hereda de la clase Vessel.
     */
    public function eliminarDatos(String $vesselClass) 
    {
        ClaseInvalidaEx::check($vesselClass, Vessel::class);
        $this->truncateTable($vesselClass);
    }

    /**
     * Importa los datos desde el servidor remoto SWAPI.
     */
    public function importarDatos(ImportarDatosQuery $importarDatosQuery)
    {
        if ($importarDatosQuery->getStarships())
            $this->importResource(Starship::class);
        if ($importarDatosQuery->getVehicles())
            $this->importResource(Vehicle::class);
    }

    /**
     * Trunca la tabla de la entidad dada.
     * @param string className
     */
    protected function truncateTable(string $className, bool $cascade = true)
    {
        /// https://stackoverflow.com/questions/9686888/how-to-truncate-a-table-using-doctrine-2
        $cmd = $this->entityManager->getClassMetadata($className);
        $connection = $this->entityManager->getConnection();
        $connection->beginTransaction();

        try {
            // https://stackoverflow.com/questions/8526534/how-to-truncate-a-table-using-doctrine
            $connection->executeUpdate($connection->getDatabasePlatform()->getTruncateTableSQL($cmd->getTableName(), $cascade));
            // https://stackoverflow.com/questions/5586269/how-can-i-reset-a-autoincrement-sequence-number-in-sqlite
            $connection->executeUpdate("UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = '" . $cmd->getTableName() . "'");
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollback();
        }
    }

    /**
     * Borra los datos de la entidad data.
     */
    protected function clearTable(string $className)
    {
        // https://developer-paradize.blogspot.com/2015/11/how-to-truncate-table-with-symfony-and.html
        $this->entityManager->createQuery('DELETE FROM ' . $className)->execute();
    }
    
    protected function importResource(string $resource)
    {
        
        $this->truncateTable($resource);

        // https://www.doctrine-project.org/projects/doctrine-orm/en/2.8/reference/batch-processing.html
        $batchSize = 20;
        $i = 0;
        $resources = $this->fetchResource($resource);
        foreach ($resources as $resource) 
        {
            $i++;
            $this->entityManager->persist($resource);
            if ($i  === $batchSize) {
                $this->entityManager->flush();
                $this->entityManager->clear(); // Detaches all objects from Doctrine!
                $i = 0;
            }
        }
        $this->entityManager->flush(); //Persist objects that did not make up an entire batch
        $this->entityManager->clear();
    }

    /**
     * Obtiene la Url para el recurso expecífico.
     * @return string url
     */
    protected function getResourceUrl(string $resource) : string
    {
        return $this->swapiUrl . '/api/' . self::RESOURCE[$resource];
    }

    /**
     * Obtiene un array de recursos remotos.
     * @return array resources
     */
    public function fetchResource(string $resource) : array
    {
        $data = [];
        $resourceUrl = $this->getResourceUrl($resource) . '/?page=1';
        while ($resourceUrl)
        {
            $content = file_get_contents($resourceUrl);
            $response = $this->deserializeFromJson($content, self::RESOURCE_RESPONSE[$resource]);
            $data = array_merge($data, $response->results);
            $resourceUrl = $response->next;
        }
        return $data;
    }

    public function deserialize($data, $class, $format, array $context = [])
    {
        return $this->serializer->deserialize($data, $class, $format, $context);
    }

    public function deserializeFromJson(string $json, string $className)
    {
        return $this->deserialize($json, $className, 'json');
    }


}