<?php
namespace App\Service;

/**
 * Excepción originada cuando el valor ingresado en negativo.
 */
class CantidadInvalidaEx extends AppException
{

    /**
     * Valor que ha originado la excepción.
     *
     * @var integer 
     */
    private $cantidad;
    public function __construct(int $cantidad){
        parent::__construct('La cantidad ingresada debe ser cero o un número positivo. Valor dado: ' . $cantidad);
        $this->cantidad = $cantidad;
    } 

    /**
     * Obtiene la cantidad que ha originado la excepción.
     *
     * @return integer el valor que ha originado la excepción.
     */
    public function getCantidad(): int
    {
        return $this->cantidad;
    }

    /**
     * Verifica que la cantidad ingresada sea cero o un valor positivo
     *
     * @param integer $cantidad valor a verificar.
     * @return integer la cantidad ingresada luego de verificar si es válida.
     * 
     * @throws CantidadInvalidaEx si el valor ingresado es un número negativo.
     */
    public static function check(int $cantidad): int
    {
        if ($cantidad < 0)
            throw new CantidadInvalidaEx($cantidad);
        return $cantidad;
    }
}