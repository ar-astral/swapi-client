<?php
namespace App\Service;

/**
 * Representa la Clase Excepción base de la Aplicación.
 */
abstract class AppException extends \Exception
{

}